#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <linux/kd.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

inline uint32_t pixel_color(uint8_t r, uint8_t g, uint8_t b,
                            struct fb_var_screeninfo *vinfo) {
  return (r << vinfo->red.offset) | (g << vinfo->green.offset) |
    (b << vinfo->blue.offset);
}

int main()
{
  // Set TTY to graphics mode
  int tty_fd = open("/dev/tty0", O_RDWR);
  ioctl(tty_fd, KDSETMODE, KD_GRAPHICS);
  
  // Open framebuffer for reading & writing
  int fb_fd = open("/dev/fb1", O_RDWR);
  if (!fb_fd) {
    fprintf(stderr, "Error: cannot open framebuffer device.\n");
    exit(1);
  }
  printf("The framebuffer device was opened successfully.\n");

  // Get fixed screen information
  struct fb_fix_screeninfo finfo;
  if (ioctl(fb_fd, FBIOGET_FSCREENINFO, &finfo)) {
    fprintf(stderr, "Error reading fixed information.\n");
    exit(2);
  }

  // Get variable screen information
  struct fb_var_screeninfo vinfo;
  if (ioctl(fb_fd, FBIOGET_VSCREENINFO, &vinfo)) {
    fprintf(stderr, "Error reading variable information.\n");
    exit(3);
  }

  // Set VSCREENINFO to something reasonable (i.e. 32-bit color mode)
  vinfo.grayscale = 0;
  vinfo.bits_per_pixel = 32;
  if (ioctl(fb_fd, FBIOPUT_VSCREENINFO, &vinfo)) {
    if (ioctl(fb_fd, FBIOPUT_VSCREENINFO, &vinfo)) {
      fprintf(stderr, "Error setting variable information.\n");
      exit(4);
    }
  }

  // Determine screensize
  long screensize = vinfo.yres_virtual * finfo.line_length;

  // Map the device to memory
  uint8_t *fbp = (char *)mmap(0,
                              screensize,
                              PROT_READ|PROT_WRITE,
                              MAP_SHARED,
                              fb_fd,
                              0);

  if ((int64_t)fbp == -1) {
    fprintf(stderr, "Error: failed to map framebuffer device to memory.\n");
    exit(5);
  }
  printf("Framebuffer device was mapped to memory successfully.\n");
  
  // Figure out where in memory to put the pixel
  uint32_t x, y;
  long location;
  for (x=0; x<(vinfo.xres); x++) {
    for (y=0; y<vinfo.yres; y++) {
      location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) +
        (y+vinfo.yoffset) * finfo.line_length;
      *((uint32_t*)(fbp + location)) = pixel_color(0xFF, 0x00, 0xFF, &vinfo);
    }
  }

  sleep(2);

  munmap(fbp, screensize);
  close(fb_fd);

  // Set TTY back to text
  ioctl(tty_fd, KDSETMODE, KD_TEXT);
  close(tty_fd);
  
  return 0;
}
